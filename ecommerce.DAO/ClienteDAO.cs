﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecommerce.DTO;
using System.Data;
using System.Data.SqlClient;

namespace ecommerce.DAO
{
    public static class ClienteDAO
    {
        /// <summary>
        /// Trae todos los registros de los clientes con la clausula WHERE especifica.
        /// </summary>
        /// <param name="where">Clausula WHERE que va a ser aplicada al query.</param>
        /// <returns>
        /// Una lista de DTO's de Articulos.
        /// </returns>
        public static List<ClienteDTO> ReadAll(string where)
        {
            var dt = DAOHelper.HacerQuery("SELECT * FROM Clientes " + where);
            List<ClienteDTO> lista = new List<ClienteDTO>();
            //Itero entre los registros para armar la Lista de DTO.
            foreach (DataRow dr in dt.Rows)
            {
                ClienteDTO dto = new ClienteDTO();

                if (!dr.IsNull("Id")) dto.Id = (int)dr["Id"];
                if (!dr.IsNull("Nombre")) dto.Nombre = (string)dr["Nombre"];
                if (!dr.IsNull("Direccion")) dto.Direccion = (string)dr["Direccion"];
                if (!dr.IsNull("Telefono")) dto.Telefono = (string)dr["Telefono"];
                if (!dr.IsNull("Email")) dto.Email = (string)dr["Email"];
                if (!dr.IsNull("Contraseña")) dto.Contraseña = (string)dr["Contraseña"];
                if (!dr.IsNull("Usuario")) dto.Usuario = (string)dr["Usuario"];
                lista.Add(dto);
            }
            return lista;
        }

        /// <summary>
        /// Trae un DTO de cliente mediante el usuario.
        /// </summary>
        /// <param name="user">String con el nombre de usuario del Cliente.</param>
        /// <returns>
        /// Un DTO de cliente.
        /// </returns>
        public static ClienteDTO GetClienteByUsuario(string user)
        {
            var usuarios = ReadAll("WHERE Usuario = '" + user + "'");
            if (usuarios.Count == 0) return null;
            else return usuarios[0];
        }
        /// <summary>
        /// Da de alta un usuario.
        /// </summary>
        /// <param name="usuario">Debe ser de tipo ClienteDTO con el id vacio. Y debe cumplir las validaciones,
        /// no se comprueba la validez de los datos ingresados.</param>
        /// <returns>Devuelve true si pudo completar la operación, false en caso contrario.</returns>
        public static bool AltaUsuario(ClienteDTO usuario)
        {
            string cmdText = "INSERT INTO Clientes(Id, Nombre, Direccion, Telefono, Email, Usuario, Contraseña) VALUES(" + DAOHelper.GetNextId("Clientes") + ",'" + usuario.Nombre + "', '" + usuario.Direccion + "','" + usuario.Telefono + "', '" + usuario.Email + "','" + usuario.Usuario + "','" + usuario.Contraseña + "')";
            return DAOHelper.HacerComando(cmdText);
        }
        /// <summary>
        /// Actualiza la información de un usuario.
        /// </summary>
        /// <param name="usuario">Debe ser de tipo ClienteDTO. Y debe cumplir las validaciones,
        /// no se comprueba la validez de los datos ingresados.</param>
        /// <returns>
        /// Devuelve true si pudo completar la operación, false en caso contrario.
        /// </returns>
        public static bool UpdateUsuario(ClienteDTO usuario)
        {
            string cmdText = "";
            cmdText = "UPDATE Clientes SET Nombre = '{0}', Direccion = '{1}', Telefono = '{2}', Email = '{3}', Usuario = '{4}', Contraseña = '{5}' WHERE Id = {6}";
            cmdText = string.Format(cmdText, usuario.Nombre, usuario.Direccion, usuario.Telefono, usuario.Email, usuario.Usuario, usuario.Contraseña, usuario.Id);
            return DAOHelper.HacerComando(cmdText);
        }
        /// <summary>
        /// Elimina a un usuario de la tabla de clientes.
        /// </summary>
        /// <param name="id">Id del usuario a eliminar</param>
        /// <returns>
        /// Devuelve True si pudo completarlo y false en caso contrario.
        /// </returns>
        public static bool DeleteUsuario(int id)
        {
            string cmdText = "";
            cmdText = "DELETE FROM Clientes WHERE Id = {0}";
            cmdText = string.Format(cmdText, id);
            return DAOHelper.HacerComando(cmdText);
        }
    }
}
