﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecommerce.DTO;
using System.Data;
using System.Data.SqlClient;
using ecommerce.DAO;
using System.Reflection;

namespace ecommerce.DAO
{
    public static class ArticuloDAO
    {
        public static List<ArticuloDTO> ReadAll(string where)
        {
            DataTable dt = new DataTable();

            //Leo los registros de la DB.
            using (SqlDataAdapter da = new SqlDataAdapter(
                "SELECT * FROM ARTICULOS " + where,
                DAOHelper.connectionString))
            {
                da.Fill(dt);
            }
            ArticuloDTO dto;
            List<ArticuloDTO> lista = new List<ArticuloDTO>();

            //Itero entre los registros para armar la Lista de DTO.
            foreach (DataRow dr in dt.Rows)
            {
                dto = new ArticuloDTO();


                //foreach (PropertyInfo prop in dto.GetType().GetProperties())
                //{
                //    if (!dr.IsNull(prop.Name))
                //    {
                //        prop.SetValue(dto, dr[prop.Name]);
                //    }
                //}

                if (!dr.IsNull("Id")) dto.Id = (int)dr["Id"];
                if (!dr.IsNull("Nombre")) dto.Nombre = (string)dr["Nombre"];
                if (!dr.IsNull("Descripcion")) dto.Descripcion = (string)dr["Descripcion"];
                if (!dr.IsNull("PrecioVenta")) dto.PrecioVenta = (decimal)dr["PrecioVenta"];
                if (!dr.IsNull("PrecioCompra")) dto.PrecioCompra = (decimal)dr["PrecioCompra"];
                if (!dr.IsNull("Stock")) dto.Stock = (int)dr["Stock"];

                lista.Add(dto);
            }

            return lista;
        }

        public static bool SePuedenComprar(int IdArticulo, int cantidad)
        {
            var articulo = ReadAll("WHERE Id = " + IdArticulo);
            if (articulo[0].Stock < cantidad)
            {
                return false;
            }
            return true;
        }



        public static ArticuloDTO GetById(int id)
        {
            var articulos = ReadAll("WHERE Id = " + id);
            if (articulos.Count == 1) return articulos[0];
            else return null;
        }

        public static List<ArticuloDTO> FiltrarPorNombre(string nombre)
        {
            return ReadAll(string.Format("WHERE nombre LIKE '%{0}%'", nombre));
        }

        public static int Update(ArticuloDTO articulo)
        {
            int rowsAffected = 0;

            using (SqlConnection conn = new SqlConnection(DAOHelper.connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE Articulos SET Nombre='[nombre]', Descripcion='[descripcion]', PrecioVenta=[precioVenta], PrecioCompra=[precioCompra], Stock='[stock]' WHERE Id=" + articulo.Id;

                    //Reemplazo los valores de los campos en el query.
                    cmd.CommandText = cmd.CommandText.Replace("[nombre]", articulo.Nombre);
                    cmd.CommandText = cmd.CommandText.Replace("[descripcion]", articulo.Descripcion);
                    cmd.CommandText = cmd.CommandText.Replace("[precioVenta]", articulo.PrecioVenta.ToString(System.Globalization.CultureInfo.InvariantCulture));       //El "InvariantCulture" es para que el ToString() ponga los decimales con '.' en lugar de ','.
                    cmd.CommandText = cmd.CommandText.Replace("[precioCompra]", articulo.PrecioCompra.ToString(System.Globalization.CultureInfo.InvariantCulture));     //El "InvariantCulture" es para que el ToString() ponga los decimales con '.' en lugar de ','.
                    cmd.CommandText = cmd.CommandText.Replace("[stock]", articulo.Stock.ToString());

                    //Ejecuto el update.
                    rowsAffected = cmd.ExecuteNonQuery();
                }
            }

            return rowsAffected;
        }

        public static void RestarArticulo(int idarticulo, int cantidad)
        {
            var articulo = GetById(idarticulo);
            int nuevoStock = articulo.Stock - cantidad;

            string cmdtext = "UPDATE Articulos SET Stock = {0} WHERE Id = {1}";
            DAOHelper.HacerComando(string.Format(cmdtext, nuevoStock, idarticulo));
        }

        public static ArticuloDTO Create(ArticuloDTO articulo)
        {
            int rowsAffected = 0;

            using (SqlConnection conn = new SqlConnection(DAOHelper.connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"INSERT INTO Articulos (Id, Nombre, Descripcion, PrecioVenta, PrecioCompra, Stock) 
                                        VALUES ([id],'[nombre]','[descripcion]',[precioVenta],[precioCompra],[stock])";

                    int proximoId = DAOHelper.GetNextId("Articulos");
                    articulo.Id = proximoId;

                    //Reemplazo los valores de los campos en el query.
                    cmd.CommandText = cmd.CommandText.Replace("[id]", proximoId.ToString());
                    cmd.CommandText = cmd.CommandText.Replace("[nombre]", articulo.Nombre);
                    cmd.CommandText = cmd.CommandText.Replace("[descripcion]", articulo.Descripcion);
                    cmd.CommandText = cmd.CommandText.Replace("[precioVenta]", articulo.PrecioVenta.ToString());
                    cmd.CommandText = cmd.CommandText.Replace("[precioCompra]", articulo.PrecioCompra.ToString());
                    cmd.CommandText = cmd.CommandText.Replace("[stock]", articulo.Stock.ToString());

                    //Ejecuto el update.
                    rowsAffected = cmd.ExecuteNonQuery();
                }
            }

            return articulo;
        }

        public static int Delete(int id)
        {
            int rowsAffected = 0;

            using (SqlConnection conn = new SqlConnection(DAOHelper.connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "DELETE Articulos WHERE Id=" + id;

                    //Ejecuto el delete.
                    rowsAffected = cmd.ExecuteNonQuery();
                }
            }

            return rowsAffected;
        }
    }
}
