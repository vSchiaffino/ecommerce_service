﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using ecommerce.DTO;

namespace ecommerce.DAO
{
    public static class DAOHelper
    {
        internal static string connectionString = @"Server=sql5027.site4now.net;Database=DB_9CF8B6_VentasDB;User Id=DB_9CF8B6_VentasDB_admin;Password=huergo2019;";
        // internal static string connectionString = @"Server=DESKTOP-KP13QA8\SQLEXPRESS;Database=VentasDB;Trusted_Connection=True;";
        // internal static string connectionString = @"Server=TESLA-20\LOCALDB#C508A7E2;Database=VentasDB;Trusted_Connection=True;";


        //Get next id genérico.
        internal static int GetNextId(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT IsNull(MAX(Id),0) FROM " + tableName;

                    int ultimoId = (int)cmd.ExecuteScalar();

                    return ultimoId + 1;
                }
            }
        }

        public static void TEST()
        {
            //DataRow dr = new DataRow();
            //ArticuloDTO articulo = new ArticuloDTO();

            //BuildDTO<ArticuloDTO>(articulo, dr);

            //BuildDTO<VentaDTO>(new VentaDTO(), dr);
        }

        public static void BuildDTO<T>(T dto, DataRow dr)
        {

            //dto.GetType().

        }

        public static DataTable HacerQuery(string command)
        {
            DataTable dt = new DataTable();
            // Leo los registros de la DB.
            using (SqlDataAdapter da = new SqlDataAdapter(
                command,
                DAOHelper.connectionString))
            {
                da.Fill(dt);
            }
            return dt;
        }

        public static bool HacerComando(string cmdText)
        {
            using (SqlConnection conn = new SqlConnection(DAOHelper.connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = cmdText;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }

                }
            }
        }
    }
}
