﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ecommerce.DTO;
using ecommerce.DAO;

namespace ecommerce.DAO
{
    public static class VentasCabeceraDAO
    {
        public static List<VentaCabeceraDTO> ReadAll(string where)
        {
            var dt = DAOHelper.HacerQuery("SELECT * FROM VentasCabecera " + where);
            List<VentaCabeceraDTO> lista = new List<VentaCabeceraDTO>();
            foreach (DataRow dr in dt.Rows)
            {
                VentaCabeceraDTO item = new VentaCabeceraDTO();
                if (!dr.IsNull("Id")) item.Id = (int)dr["Id"];
                if (!dr.IsNull("Id")) item.Fecha = (DateTime)dr["Fecha"];
                if (!dr.IsNull("IdCliente")) item.IdCliente = (int)dr["IdCliente"];
                if (!dr.IsNull("IdVendedor")) item.IdVendedor = (int)dr["IdVendedor"];
                if (!dr.IsNull("Observaciones")) item.Observaciones = (string)dr["Observaciones"];
                lista.Add(item);
            }
            return lista;
        }

        public static List<VentaCabeceraDTO> GetVentasUsuario(ClienteDTO user)
        {
            return ReadAll("WHERE IdCliente = " + user.Id);
        }

        public static bool AltaVenta(VentaCabeceraDTO vc)
        {
            string cmdtext = "INSERT INTO VentasCabecera(Id, Fecha, IdCliente, IdVendedor, Observaciones) VALUES({0}, '{1}', {2}, {3}, '{4}')";
            vc.Id = DAOHelper.GetNextId("VentasCabecera");
            cmdtext = string.Format(cmdtext, vc.Id, vc.Fecha.ToString("yyyy-MM-dd"), vc.IdCliente, vc.IdVendedor, vc.Observaciones);
            try
            {
                DAOHelper.HacerComando(cmdtext);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
