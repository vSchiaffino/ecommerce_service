﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ecommerce.DTO;

namespace ecommerce.DAO
{
    public static class VentasDetalleDAO
    {
        public static List<VentaDetalleDTO> ReadAll(string where)
        {
            var dt = DAOHelper.HacerQuery("SELECT * FROM VentasDetalle " + where);
            List<VentaDetalleDTO> lista = new List<VentaDetalleDTO>();
            foreach (DataRow dr in dt.Rows)
            {
                VentaDetalleDTO item = new VentaDetalleDTO();
                if (!dr.IsNull("Id")) item.Id = (int)dr["Id"];
                if (!dr.IsNull("IdVentaCabecera")) item.IdVentaCabecera = (int)dr["IdVentaCabecera"];
                if (!dr.IsNull("IdArticulo")) item.IdArticulo = (int)dr["IdArticulo"];
                if (!dr.IsNull("PrecioUnitario")) item.PrecioUnitario = (decimal)dr["PrecioUnitario"];
                if (!dr.IsNull("Cantidad")) item.Cantidad = (int)dr["Cantidad"];
                lista.Add(item);
            }
            return lista;
        }
  
        public static List<VentaDetalleDTO> GetVentasByIdVC(int idVC)
        {
            return ReadAll("WHERE IdVentaCabecera = " + idVC);
        }

        public static bool AltaVentaDetalle(VentaDetalleDTO vd, int idVc)
        {
            string cmdtext = "INSERT INTO VentasDetalle(Id, IdVentaCabecera, IdArticulo, PrecioUnitario, Cantidad) VALUES({0}, {1}, {2}, {3}, {4})";
            cmdtext = string.Format(cmdtext, DAOHelper.GetNextId("VentasDetalle"), idVc, vd.IdArticulo, vd.PrecioUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture), vd.Cantidad);
            if (DAOHelper.HacerComando(cmdtext))
            {
                ArticuloDAO.RestarArticulo(vd.IdArticulo, vd.Cantidad);
                return true;
            }
            return false;
        }

        public static bool PuedoHacerEstasVentas(List<VentaDetalleDTO> vds)
        {
            foreach(VentaDetalleDTO vd in vds)
            {
                if (!ArticuloDAO.SePuedenComprar(vd.IdArticulo, vd.Cantidad)) return false;
            }
            return true;
        }
    }
}
