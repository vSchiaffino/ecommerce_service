﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecommerce.DTO;
using ecommerce.DAO;

namespace ecommerce.DAO
{
    public static class VentaDAO
    {
        public static List<VentaDTO> GetVentasUsuario(ClienteDTO user)
        {
            var lista = new List<VentaDTO>();
            var vcs = VentasCabeceraDAO.GetVentasUsuario(user);
            foreach(VentaCabeceraDTO vc in vcs)
            {
                var venta = new VentaDTO();
                var vds = VentasDetalleDAO.GetVentasByIdVC(vc.Id);
                venta.VentaCabecera = vc;
                venta.VentasDetalle = vds;
                lista.Add(venta);
            }
            return lista;
        }

        public static bool AltaVenta(VentaDTO venta)
        {
            try
            {
                if (!VentasDetalleDAO.PuedoHacerEstasVentas(venta.VentasDetalle))
                {
                    return false;
                }
                venta.VentaCabecera.Id = DAOHelper.GetNextId("VentasCabecera");
                if (!VentasCabeceraDAO.AltaVenta(venta.VentaCabecera)) return false;
                foreach(VentaDetalleDTO vd in venta.VentasDetalle)
                {
                    VentasDetalleDAO.AltaVentaDetalle(vd, venta.VentaCabecera.Id);
                }
                return true;
            }
            catch
            {
                return false;
            }


        }
    }
}
