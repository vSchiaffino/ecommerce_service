﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ecommerce.DTO;
using ecommerce.DAO;

namespace ecommerce.Service
{
    /// <summary>
    /// Este web service se creó para el funcionamiento de una web tipo ecommerce.
    /// </summary>
    [WebService(Namespace = "http://ventasservice.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class ecommerce_service : System.Web.Services.WebService
    {
        /// <summary>
        /// Llama a todos los artículos de la tabla.
        /// </summary>
        /// <returns>
        /// Una lista de ArticulosDTO
        /// </returns>
        [WebMethod]
        public List<ArticuloDTO> GetAllArticulos()
        {
            return ArticuloDAO.ReadAll("");
        }
        [WebMethod]
        public List<ArticuloDTO> FiltrarArticulos(string nombre)
        {
            return ArticuloDAO.FiltrarPorNombre(nombre);
        }
        /// <summary>
        /// Intenta hacer login con un usuario y una contraseña dada.
        /// </summary>
        /// <param name="usuario">Debe ser un string que hace referencia al usuario que se intenta loguear</param>
        /// <param name="contraseña">Debe ser un string que hace referencia a la contraseña del usuario que se intenta loguear</param>
        /// <returns>
        /// Devuelve un ClienteDTO si pudo loguearse. Si no se pudo loguear devuelve null.
        /// </returns>
        [WebMethod]
        public ClienteDTO Login(string usuario, string contraseña)
        {
            var User = ClienteDAO.GetClienteByUsuario(usuario);
            if (User != null)
            {
                if (User.Contraseña == contraseña) return User;
            }
            return null;
        }
        /// <summary>
        /// Pone un nuevo registro en la base de datos de clientes. También valida la información para
        /// que cumpla con las validaciones establecidas.
        /// </summary>
        /// <param name="usuario">El usuario a registrar. Debe cumplir con las validaciones establecidas y tener el id vacio.</param>
        /// <returns>Devuelve un string dummy vacio si completó correctamente la operación,
        /// devuelve un string informando el error si es que hubo. Y si hubo un error pero
        /// no sabe cual devuelve "error"</returns>
        [WebMethod]
        public string AltaUsuario(ClienteDTO usuario)
        {
            var respuesta = "";
            // Comprobaciones

            // Alta
            if (respuesta != "") return respuesta;
            if (ClienteDAO.AltaUsuario(usuario)) return respuesta;
            else return "error";
        }
        /// <summary>
        /// Actualiza un registro de la base de datos de clientes. Valida la información para que cumpla
        /// con las validaciones establecidas.
        /// </summary>
        /// <param name="usuario">El usuario a actualizar. Debe cumplir con las validaciones.</param>
        /// <returns>
        /// Devuelve un string dummy vacio si completó correctamente la operación,
        /// devuelve un string informando el error si es que hubo. Y si hubo un error pero
        /// no sabe cual devuelve "error"
        /// </returns>
        [WebMethod]
        public string UpdateUsuario(ClienteDTO usuario)
        {
            var respuesta = "";
            // Comprobaciones

            // Alta
            if (respuesta != "") return respuesta;
            if (ClienteDAO.UpdateUsuario(usuario)) return respuesta;
            else return "error";
        }

        /// <summary>
        /// Devuelve todas las ventas de un determinado usuario.
        /// </summary>
        /// <param name="usuario">
        /// ClienteDTO del usuario específico.
        /// </param>
        /// <returns>
        /// Devuelve una Lista de VentaDTO.
        /// VentaDTO es un DTO que contiene una venta cabecera
        /// y todas las ventasDetalle que corresponden al VentaCabecera.
        /// </returns>
        [WebMethod]
        public List<VentaDTO> GetVentasUsuario(ClienteDTO usuario)
        {
            return VentaDAO.GetVentasUsuario(usuario);
        }
        [WebMethod]
        public ArticuloDTO GetArticuloById(int id)
        {
            return ArticuloDAO.GetById(id);
        }
        [WebMethod]
        public bool AltaVenta(VentaDTO venta)
        {
            return VentaDAO.AltaVenta(venta);
        }
    }
}
