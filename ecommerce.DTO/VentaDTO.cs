﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ecommerce.DTO;

namespace ecommerce.DTO
{
    public class VentaDTO
    {
        // Esta clase la pense para poder tener en un solo DTO todo lo necesario para mostrar una venta.
        public VentaCabeceraDTO VentaCabecera { get; set;}
        public List<VentaDetalleDTO> VentasDetalle { get; set;}
    }
}
